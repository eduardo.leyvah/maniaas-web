from flask import Flask, render_template, request, jsonify
import jinja2
from os import walk
import os
import json
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.plugins.callback import CallbackBase
import ansible.constants as C


class ResultCallback(CallbackBase):
    def __init__(self, *args, **kwargs):
        super(ResultCallback, self).__init__(*args, **kwargs)
        self.host_ok = {}
        self.host_unreachable = {}
        self.host_failed = {}
        self.results = None

    def v2_runner_on_unreachable(self, result):
        self.host_unreachable[result._host.get_name()] = result
        host = result._host
        print(json.dumps({host.name: result._result}, indent=4))
        self.results = "False"
        # return("Error. No se pudo establecer la conexión con el host")

    def v2_runner_on_ok(self, result, *args, **kwargs):
        self.host_ok[result._host.get_name()] = result
        host = result._host
        print(json.dumps({host.name: result._result}, indent=4))
        self.results = "True"
        # return("Dependencias instaladas correctamente")

    def v2_runner_on_failed(self, result, *args, **kwargs):
        self.host_failed[result._host.get_name()] = result
        host = result._host
        print(json.dumps({host.name: result._result}, indent=4))
        self.results = "False2"
        # return("Error. Dependencias no instaladas")


app = Flask(__name__)


@app.route('/')
def login():
    return render_template('index.html')


@app.route('/registro')
def registro():
    return render_template('registro.html')


@app.route('/admin-vm')
def imprimeDependencias():
    ruta = '/Users/eduardoleyvah/Developer/python-functions/testPython/maniaas-flask/roles'
    dir, subdirs, archivos = next(walk(ruta))
    subdirs[:] = [d for d in subdirs if not d.startswith('.')]
    return render_template('admin-vm.html', subdirs=subdirs)


@app.route('/creaPB', methods=["POST"])
def creaPB():
    arrayPB = request.json
    file = open("playbook.yml", "w")
    file.write("---" + os. linesep)
    file.write("- hosts: all" + os. linesep)
    file.write("  become: yes" + os. linesep)
    file.write("  roles:" + os. linesep)
    for dependencia in arrayPB:
        file.write("    - " + dependencia + os.linesep)
    file. close()
    return("Playbook Creado")


@app.route('/crearHost', methods=["POST"])
def creaHost():
    host = request.args.get('host', '')
    ssh_user = request.args.get('sshUser', '')
    ssh_pass = request.args.get('sshPass', '')
    sudo_pass = request.args.get('sudoPass', '')
    file = open("hosts", "w")
    file.write("[webserver]" + os. linesep)
    file.write(host + os.linesep)
    file.write("[webserver:vars]" + os. linesep)
    file.write("ansible_conection=ssh" + os. linesep)
    file.write("ansible_user=" + ssh_user + os. linesep)
    file.write("ansible_ssh_pass=" + ssh_pass + os. linesep)
    file.write("ansible_sudo_pass=" + sudo_pass + os. linesep)
    file.close()
    return("Inventario creado")


@app.route('/aplicaPB')
def aplicaPlaybook():
    try:
        loader = DataLoader()

        inventory = InventoryManager(loader=loader, sources=[
                                     '/Users/eduardoleyvah/Developer/python-functions/testPython/maniaas-flask/hosts'])
        variable_manager = VariableManager(loader=loader, inventory=inventory)
        passwords = {}
        results_callback = ResultCallback()
        Options = namedtuple('Options',
                             ['connection',
                              'remote_user',
                              'ask_sudo_pass',
                              'verbosity',
                              'ack_pass',
                              'module_path',
                              'forks',
                              'become',
                              'become_method',
                              'become_user',
                              'check',
                              'listhosts',
                              'listtasks',
                              'listtags',
                              'syntax',
                              'sudo_user',
                              'sudo',
                              'diff'])
        options = Options(connection='ssh',
                          remote_user=None,
                          ack_pass=None,
                          sudo_user=None,
                          forks=5,
                          sudo=None,
                          ask_sudo_pass=True,
                          verbosity=5,
                          module_path=None,
                          become=None,
                          become_method='sudo',
                          become_user='root',
                          check=False,
                          diff=False,
                          listhosts=None,
                          listtasks=None,
                          listtags=None,
                          syntax=None)

        playbook = PlaybookExecutor(playbooks=['/Users/eduardoleyvah/Developer/python-functions/testPython/maniaas-flask/playbook.yml'], inventory=inventory,
                                    variable_manager=variable_manager, loader=loader, options=options, passwords=passwords)
        playbook._tqm._stdout_callback = results_callback
        playbook.run()
        results = results_callback.results
        return(results)
    except Exception as e:
        return(str(e))

    finally:
        file = open("hosts", "w").close()
        file = open("playbook.yml", "w").close()


if __name__ == "__main__":
    app.run(debug=True)
